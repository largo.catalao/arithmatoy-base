# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return "S" * n + "0"


def S(n: str) -> str:
    return "S" + n


def addition(a: str, b: str) -> str:
    if a == "0":
        return b
    else:
        return S(addition(a[1:], b))
    

def multiplication(a: str, b: str) -> str:
    if a == "0" or b == "0":
        return "0"
    else:
        return addition(multiplication(a[1:], b), b)


def facto_ite(n: int) -> int:
    result = 1
    for i in range(1, n+1):
        result *= i
    return result


def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    else:
        return n * facto_rec(n-1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)


def fibo_ite(n: int) -> int:
    f_n_1 = 1
    f_n_2 = 0
    for _ in range(n):
        f_n = f_n_1 + f_n_2
        f_n_2 = f_n_1
        f_n_1 = f_n
    return f_n_2


def golden_phi(n: int) -> float:
    return fibo_ite(n + 1) / fibo_ite(n)




def sqrt5(n: int) -> int:
    return 2 * golden_phi(n) - 1
    


def pow(a: float, n: int) -> float:
    result = 1
    for _ in range(n):
        result *= a
    return result